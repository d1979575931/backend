package com.example.demo.services;

import com.example.demo.dao.CustomerRepository;
import com.example.demo.dao.DivisionRepository;
import com.example.demo.entities.Customer;
import com.example.demo.entities.Division;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Component
public class DataInitializer {

        @Autowired
        private CustomerRepository customerRepository;

        @Autowired
        private DivisionRepository divisionRepository;

        @Transactional
        @PostConstruct
        public void initializeData() {

            Division defaultDivision = divisionRepository.findAll().stream().findFirst().orElse(null);
             if (defaultDivision == null) {
                 return;
             }



            if(customerRepository.count() <= 1) {
                Customer customer1 = new Customer();
                customer1.setFirstName("John");
                customer1.setLastName("Williams");
                customer1.setAddress("123 sunset cove");
                customer1.setPostal_code("12345");
                customer1.setPhone("123-456-7890");
                customer1.setCreate_date(new Date());
                customer1.setLast_update(new Date());
                customer1.setDivision(defaultDivision);
                customerRepository.save(customer1);

                Customer customer2 = new Customer();
                customer2.setFirstName("Jack");
                customer2.setLastName("Williams");
                customer2.setAddress("1000 sunset cove");
                customer2.setPostal_code("21345");
                customer2.setPhone("162-456-7890");
                customer2.setCreate_date(new Date());
                customer2.setLast_update(new Date());
                customer2.setDivision(defaultDivision);
                customerRepository.save(customer2);

                Customer customer3 = new Customer();
                customer3.setFirstName("Jason");
                customer3.setLastName("Williams");
                customer3.setAddress("150 sunset cove");
                customer3.setPostal_code("31345");
                customer3.setPhone("152-456-7890");
                customer3.setCreate_date(new Date());
                customer3.setLast_update(new Date());
                customer3.setDivision(defaultDivision);
                customerRepository.save(customer3);

                Customer customer4 = new Customer();
                customer4.setFirstName("Rob");
                customer4.setLastName("Williams");
                customer4.setAddress("200 sunset cove");
                customer4.setPostal_code("41345");
                customer4.setPhone("142-456-7890");
                customer4.setCreate_date(new Date());
                customer4.setLast_update(new Date());
                customer4.setDivision(defaultDivision);
                customerRepository.save(customer4);

                Customer customer5 = new Customer();
                customer5.setFirstName("Mark");
                customer5.setLastName("Williams");
                customer5.setAddress("500 sunset cove");
                customer5.setPostal_code("51345");
                customer5.setPhone("132-456-7890");
                customer5.setCreate_date(new Date());
                customer5.setLast_update(new Date());
                customer5.setDivision(defaultDivision);
                customerRepository.save(customer5);



            }

        }
}

