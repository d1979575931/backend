package com.example.demo.services;

import com.example.demo.entities.Cart;
import com.example.demo.entities.CartItem;
import java.util.Set;

import com.example.demo.entities.Customer;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class Purchase {

    private Cart cart;
    private Set<CartItem> cartItems;
    private Customer customer;
}
